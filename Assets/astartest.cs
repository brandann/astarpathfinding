﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class astartest : AstarTileNode
{

	public GameObject nodetile;
	public GameObject HeroPosition;
	public GameObject EndPosition;
	private bool[,] BoolMap;

	// Use this for initialization
	void Start ()
	{
		StartBool();
		SetHeroPosition(HeroPosition.transform.position, EndPosition.transform.position);
	}

	private void SetHeroPosition(Vector3 hero, Vector3 end)
	{
		HeroPosition.transform.position = hero;
		EndPosition.transform.position = end;
	}

	void Update ()
	{
		int hx = (int) HeroPosition.transform.position.x;
		int hy = (int) HeroPosition.transform.position.y;

		if(Input.GetKeyUp(KeyCode.UpArrow))
			hy++;
		if (Input.GetKeyUp(KeyCode.DownArrow))
			hy--;
		if (Input.GetKeyUp(KeyCode.LeftArrow))
			hx--;
		if (Input.GetKeyUp(KeyCode.RightArrow))
			hx++;
		
		int ex = (int)EndPosition.transform.position.x;
		int ey = (int)EndPosition.transform.position.y;

		if (Input.GetKeyUp(KeyCode.W))
			ey++;
		if (Input.GetKeyUp(KeyCode.S))
			ey--;
		if (Input.GetKeyUp(KeyCode.A))
			ex--;
		if (Input.GetKeyUp(KeyCode.D))
			ex++;

		var hero = HeroPosition.transform.position;
		var end = EndPosition.transform.position;

		if (BoolMap[hy, hx])
		{
			hero = new Vector3(hx, hy, 0);
		}
		if (BoolMap[ey, ex])
		{
			end = new Vector3(ex, ey, 0);
		}

		SetHeroPosition(hero, end);

		//get all nodes from world space
		var wsnodes = GameObject.FindGameObjectsWithTag("AstarNode");

		var path = AstarPathfinding.GetShortestPath(wsnodes, HeroPosition.transform.position, EndPosition.transform.position);
		PaintPath(path);
	}
	
	private void StartBool()
	{
		var T = true;
		var F = false;

		BoolMap = new bool[,]
		{
			{F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F},
			{F,F,T,T,T,T,T,T,T,T,T,F,T,T,T,F},
			{F,F,T,T,T,F,F,F,F,T,T,F,T,F,T,F},
			{F,F,T,T,T,F,F,F,F,T,T,F,F,F,T,F},
			{F,F,T,T,T,F,T,T,T,T,T,T,T,T,T,F},
			{F,F,F,F,F,F,T,T,T,T,T,F,F,F,F,F},
			{F,F,F,F,F,F,T,T,F,F,T,F,T,T,T,F},
			{F,T,T,T,T,T,T,T,F,F,T,F,F,F,T,F},
			{F,T,T,T,F,F,T,T,T,T,T,T,F,F,T,F},
			{F,F,F,T,T,F,T,T,T,F,F,T,F,F,T,F},
			{F,F,F,T,T,T,T,T,T,F,F,T,F,F,T,F},
			{F,F,F,T,T,T,T,T,T,F,F,T,T,T,T,F},
			{F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F}
		};

		for (int i = 0; i < BoolMap.GetLength(0); i++)
		{
			for (int j = 0; j < BoolMap.GetLength(1); j++)
			{
				var go = GameObject.Instantiate(nodetile);
				var sprite = go.GetComponent<SpriteRenderer>();
				go.transform.position = new Vector3(j, i, 0);
				var nodet = go.GetComponent<AstarTileNode>();

				if (BoolMap[i, j])
				{
					sprite.color = Color.cyan;
					nodet.AstarOpenTile = AstarPathfinding.OPEN;
				}
				else
				{
					sprite.color = Color.gray;
					nodet.AstarOpenTile = AstarPathfinding.CLOSED;
				}
			}
		}
	}
	
	private void PaintPath(AstarNode path)
	{
		//get all nodes from world space
		var wsnodes = GameObject.FindGameObjectsWithTag("AstarNode");

		var current = path;

		foreach (var n in wsnodes)
		{
			if (n.GetComponent<AstarTileNode>().AstarOpenTile)
				n.GetComponent<SpriteRenderer>().color = Color.cyan;
			else
				n.GetComponent<SpriteRenderer>().color = Color.grey;
		}

		while (current != null)
		{
			foreach (var n in wsnodes)
			{
				int x = (int)n.transform.position.x;
				int y = (int)n.transform.position.y;
				var v = new Vector2(x, y);
				if (v == current.Position)
					n.GetComponent<SpriteRenderer>().color = Color.blue;
			}
			current = current.ParentNode;
		}
	}
}
